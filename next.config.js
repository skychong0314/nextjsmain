const express = require('express');
const next = require('next');
const dev = process.env.NODE_ENV !== 'production';

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
}

const port = process.env.PORT || 3010;
const app = next({dev: true});
const handle = app.getRequestHandler();


app.prepare().then(() => {
  const server = express();

  server.use(express.json());

  // Login Info
  server.post('/api/LoginMainPage', (req, res) => {
    console.log("received test")
    const{email, password} = req.body;
    res.json({
      email,
      password,
      success: true
    })
  });

  // Cust Sign Up
  server.post('/api/custSignUp', (req, res) => {
    console.log("received test")
    const{email, password, firstname, lastname, dateofbirth, telphone} = req.body;
    res.json({
      email,
      password,
      firstname,
      lastname,
      dateofbirth,
      telphone,
      success: true
    })
  });

  server.post('/api/Reviews', (req, res) => {
    console.log("received test")
    const{rating, review, selection} = req.body;
    res.json({
      rating,
      review,
      selection,
      success: true
    })
  });

  server.get("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(port, err => {
    if(err) throw err;
    console.log(`Listening on PORT ${port}`);
  });
});

module.exports = nextConfig

module.exports = {
  experimental: {
    reactRefresh: false,
  },
}

