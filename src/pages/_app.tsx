import '@/styles/globals.css'
import '@/styles/review.css'
import "@fortawesome/fontawesome-free/css/all.css"
import type { AppProps } from 'next/app'
// add bootstrap css
import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap/dist/js/bootstrap.bundle'
import { useEffect } from 'react'






export default function App({ Component, pageProps }: AppProps) {

  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle")
  }, []);
  return <Component {...pageProps} />
}
