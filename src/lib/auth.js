import axios from 'axios';

export const loginDetails = async(email, password) => {
    const {data} = await axios.post('/api/LoginMainPage', { email, password});
    console.log(data);
    return data;
}

export const signUpPage = async(email, password, firstname, lastname, dateofbirth, telphone) => {
    const {data} = await axios.post("/api/custSignUp", {email, password, firstname, lastname, dateofbirth, telphone});
    console.log(data);
    return data;
}

export const reviewPage = async(rating, review, selection) => {
    const {data} = await axios.post("/api/Reviews", {rating, review, selection});
    console.log(data);
    return data;
}

