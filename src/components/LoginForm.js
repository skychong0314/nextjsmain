import React from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import {loginDetails} from '../lib/auth';
import { useState } from 'react';

function LoginForm() {
  const router = useRouter();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [selectedValue, setSelectedValue] = useState('');

  const handleChange = event => {
    const { name, value } = event.target;
    if (name === 'email') {
      setEmail(value);
    } else if (name === 'password') {
      setPassword(value);
    }
  };

  const handleSubmit = event => {
    event.preventDefault();
    loginDetails(email, password);

    
    if(selectedValue == 'cust'){
      console.log('customer selected');
    } else if(selectedValue == 'manager'){
      console.log('manager selected');
    } else if(selectedValue == 'owner'){
      router.push('/ownerloginpage');
    } else if(selectedValue == 'admin'){
      console.log('admin selected');
    }
  };

  const handleSelectChange = (event) => {
    setSelectedValue(event.target.value);
  }

  return (
    <>
    <div className="px-5 m-5" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', height: '100vh'}}>
      <form onSubmit={handleSubmit}> 
      <h2 className="mb-3 form-label">Welcome Back</h2>
      <label className="mb-3 form-label">I am a ...</label>
      <div className="grid gap-3">
        <select className="form-select" aria-label="Default select example" value={selectedValue} onChange={handleSelectChange}>
          <option selected disabled>Select User</option>
          <option value="cust">Customer</option>
          <option value="manager">Cinema Manager</option>
          <option value="owner">Cinema Owner</option>
          <option value="admin">User Admin</option>
        </select>
        <div className="mb-3">
        <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
        <input
          type="email"
          name="email"
          className="form-control" 
          placeholder="Email"
          value={email}
          onChange={handleChange}
          required
        />
        </div>
        <div className="mb-3">
        <label htmlfor="exampleInputPassword1" className="form-label">Password</label>
        <input
          type="password"
          name="password"
          className="form-control" 
          placeholder="Password"
          value={password}
          onChange={handleChange}
          required
        />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
      
    </>
    
  );
}

export default LoginForm;

// class LoginForm extends React.Component{

//     state = {
//         username: '',
//         password: ''
//     };

//     handleChange = event => {
//         this.setState({[event.target.name]: event.target.value});
//     }

//     handleSubmit = event =>{
//         const {username, password} = this.state;

//         event.preventDefault();
//         loginOwner(username, password);
//     }

//     render(){
//         return(
//             <form onSubmit={this.handleSubmit}>
//                 <div>
//                     <input type="username"
//                     name="username"
//                     placeholder="Username"
//                     onChange={this.handleChange}/>
//                 </div>
//                 <div>
//                     <input type="password"
//                     name="password"
//                     placeholder="Password"
//                     onChange={this.handleChange}
//                     />
//                 </div>
//                 <button type="submit">Submit</button>
//             </form>
//         )
//     }
// }

// export default LoginForm;