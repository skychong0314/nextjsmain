import { useState, useEffect } from 'react';
import { createClient } from '@supabase/supabase-js';

const supabase = createClient('https://xpqjwcqzcnzxjmujjdmc.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InhwcWp3Y3F6Y256eGptdWpqZG1jIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODIzMTU4OTcsImV4cCI6MTk5Nzg5MTg5N30.08k0Tjxtig93CR20VHzUdZaysWzE7NXggLOvMm5fHaM');

export default function CinemaPage() {
  const [movies, setMovies] = useState([]);
  const [selectedMovie, setSelectedMovie] = useState(null);

  useEffect(() => {
    const fetchMovies = async () => {
      // Fetch user ratings from Supabase
      const { data: userRatings, error } = await supabase
        .from('user_rating_review')
        .select('movie_name, rating, review');
      if (error) console.log('Error fetching movies:', error);

      // Fetch movie URLs from Supabase
      const { data: movieUrls, error: movieError } = await supabase
        .from('movies')
        .select('movie_name, movie_url');
      if (movieError) console.log('Error fetching movies:', movieError);

      // Merge movie URLs with user ratings and group ratings and reviews by movie name
      const mergedMovies = userRatings.reduce((acc, userRating) => {
        const matchingMovieUrl = movieUrls.find((movieUrl) => movieUrl.movie_name === userRating.movie_name);
        const existingMovie = acc.find((movie) => movie.movie_name === userRating.movie_name);
        if (existingMovie) {
          existingMovie.rating.push(userRating.rating);
          existingMovie.review.push(userRating.review);
        } else {
          acc.push({
            ...userRating,
            ...matchingMovieUrl,
            rating: [userRating.rating],
            review: [userRating.review],
          });
        }
        return acc;
      }, []);

      setMovies(mergedMovies);
    };

    fetchMovies();
  }, []);

  const handleMovieSelect = (event) => {
    const movieName = event.target.value;
    const selectedMovie = movies.find((movie) => movie.movie_name === movieName);
    setSelectedMovie(selectedMovie);
  };

  return (
    <div>
      
      <div className="container2">
      
      <div>
      <h2><b>Movie Review Page</b></h2>
        <select onChange={handleMovieSelect}>
          <option value="">Select a movie</option>
          {movies.map((movie) => (
            <option key={movie.movie_name} value={movie.movie_name}>
              {movie.movie_name}
            </option>
          ))}
        </select>
        {selectedMovie && (
          <div>
            <h3>{selectedMovie.movie_name}</h3>
            
            <img src={selectedMovie.movie_url} alt={selectedMovie.movie_name} width="350" height="400" />
            

           
            <div className="user-rating-box">
  <h4>User Ratings:</h4>
  {selectedMovie.rating.length > 0 ? (
    <div>
      <p><b>Number of ratings: {selectedMovie.rating.length}</b></p>
      <p><b>Average rating: {selectedMovie.rating.reduce((acc, curr) => acc + curr) / selectedMovie.rating.length}</b></p>
     
      <b> User Review: </b>
      <ul>
        
       
        {selectedMovie.review.map((review, index) => (
          <li key={index}>-{review}</li>
        ))}
      </ul>
    </div>
    
  ) : (
    <p>No ratings yet</p>
  )}
</div>
</div>
)}
</div>
</div>
</div>

);
}

export async function getStaticProps() {
// Fetch movie names from Supabase
const { data: movies, error } = await supabase.from('movies').select('movie_name');
if (error) console.log('Error fetching movies:', error);

return {
props: {
movies: movies.map((movie) => movie.movie_name),
},
};
}
