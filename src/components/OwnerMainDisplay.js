import React, { useState } from 'react';

function OwnerMainDisplay(){

    const [selectedReport, setSelectedReport] = useState('');
    const [selectedRec, setSelectedRec] = useState('');

    const handleSelectRptChange = (event) => {
        setSelectedReport(event.target.value);
    }

    const handleSelectRecChange = (event) => {
        setSelectedRec(event.target.value);
    }

    const handleSubmit = event => {
        event.preventDefault();

        if(selectedReport == 'tickets'){
            console.log('Total Tickets Sales');
            if(selectedRec == 'daily'){
                console.log('Total Tickets Sales Daily Report');
            } else if(selectedRec == 'monthly'){
                console.log('Total Tickets Sales Monthly Report');
            } else if(selectedRec == 'yearly'){
                console.log('Total Tickets Sales Yearly Report');
            }
        } else if(selectedReport == 'sales'){
            console.log('Total Sales Food Items');
            if(selectedRec == 'daily'){
                console.log('Total Sales Food Items Daily Report');
            } else if(selectedRec == 'monthly'){
                console.log('Total Sales Food Items Monthly Report');
            } else if(selectedRec == 'yearly'){
                console.log('Total Sales Food Items Yearly Report');
            }
        }
    };

    return(
        <>
        <div>
        <div className="grid gap-3 px-5 m-5" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flexi-start', height: '100vh', alignItems: 'flex-start'}}>
            <form onSubmit={handleSubmit}> 
                <h2 className="mb-3 form-label">Reports</h2>
                <div className="grid gap-3">
                    <select className="form-select" onChange={handleSelectRptChange}>
                        <option selected disabled>Select Report</option>
                        <option value="tickets">Tickets Report</option>
                        <option value="sales">Total Sales</option>
                    </select>
                    <select className="form-select" onChange={handleSelectRecChange} style={{marginBottom: "10px"}}>
                        <option selected disabled>Periodicity</option>
                        <option value="daily">Daily</option>
                        <option value="monthly">Monthly</option>
                        <option value="yearly">Yearly</option>
                    </select>
                </div>
                <div>
                    {/* DisplayReport */}
                </div>
                <div className="input-group mb-3">
                    <button type="submit" className="btn btn-primary" variant="primary" size="lg">Generate</button>
                </div>
            </form>
        </div>
        <div className="right">
            <table disabled hidden>

            </table>
        </div>
        </div>
        </>
    );
}

export default OwnerMainDisplay;