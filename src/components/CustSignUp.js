import * as React from 'react';
import Link from '@mui/material/Link';
import {useRouter} from 'next/router';
import { useState } from 'react';
import {signUpPage} from '../lib/auth';

export default function CustSignUp() {

  const router = useRouter();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [dateofbirth, setDateofbirth] = useState('');
  const [telphone, setTelphone] = useState('');

  const handleChange = event => {
  const { name, value } = event.target;
  if (name === 'email') {
    setEmail(value);
  } else if (name === 'password') {
    setPassword(value);
  } else if (name === 'firstname') {
      setFirstname(value);
  } else if (name === 'lastname') {
      setLastname(value);
  } else if (name === 'dob') {
      setDateofbirth(value);
  } else if (name === 'phone') {
      setTelphone(value);
  }
  };

  const handleSubmit = event => {
      event.preventDefault();
      signUpPage(email, password, firstname, lastname, dateofbirth, telphone);
      router.push('/LoginMainPage');
  };

  return (
    <>
    <div className='px-5 m-5'>
    <form onSubmit={handleSubmit}> 
    <div className='left' style={{float:'left', width:'48%'}}>
      <h2 className="mb-3 form-label modal-title"><b>Sign Up</b></h2>
      <div className="grid gap-3">
        <div className="mb-3">
        <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
        <input
          type="email"
          name="email"
          className="form-control" 
          required
          value={email}
          onChange={handleChange}
        />
        </div>
        <div className="mb-3">
        <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
        <input
          type="password"
          name="password"
          className="form-control" 
          required
          value={password}
          onChange={handleChange}
        />
        </div>
        <p style={{textAlign:'center'}}>By signing up, you agree to the <u>terms and conditions</u></p>
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link href="../LoginMainPage" style={{textAlign:'center'}}>
                Already have an account? Sign in
        </Link>
        </div>
    
    </div>

    <div className='right' style={{float:'right', width:'48%', marginTop:'65px'}}>
        <div className="mb-3">
            <label htmlFor="firstname" className="form-label">First Name</label>
            <input
            type="text"
            name="firstname"
            className="form-control" 
            required
            value={firstname}
            onChange={handleChange}
            />
        </div>
        <div className="mb-3">
            <label className="form-label">Last Name</label>
            <input
            type="text"
            name="lastname"
            className="form-control" 
            required
            value={lastname}
            onChange={handleChange}
            />
        </div>
        <div className="mb-3">
            <label htmlFor="dob" className="form-label">Date of Birth</label>
            <input
            type="date"
            name="dob"
            className="form-control"
            required 
            value={dateofbirth}
            onChange={handleChange}
            />
        </div>
        <div className="mb-3">
            <label htmlFor="phone" className="form-label">Phone</label>
            <input
            type="tel"
            name="phone"
            className="form-control" 
            pattern="[0-9]{8}"
            required
            value={telphone}
            onChange={handleChange}
            />
        </div>
    </div>
    </form>
    </div>
    </>
  );
}