import React, { useState } from 'react';
import { useRouter } from 'next/router';

export default function MainPage() {
  const [hovered, setHovered] = useState(false);
  const router = useRouter();

  const handleHover = () => {
    setHovered(!hovered);
  };

  const handleLogout = () => {
    router.push('/logout');
  };

  return (
    <nav className="bg-white shadow-lg">
      <div className="flex justify-between items-center py-4 px-6">
        <a className="text-2xl font-bold text-cyan-500" onClick={() => router.push('/')}>Movi<span className="text-black">es</span></a>
        <div className="flex items-center">
          <a className={`mx-2 font-semibold ${hovered ? 'text-gray-500' : 'text-black'} transition ease-in-out duration-300`} onMouseEnter={handleHover} onMouseLeave={handleHover} onClick={() => router.push('/movies')}>Movies</a>
          <a className={`mx-2 font-semibold ${hovered ? 'text-gray-500' : 'text-black'} transition ease-in-out duration-300`} onMouseEnter={handleHover} onMouseLeave={handleHover} onClick={() => router.push('/promotions')}>Promotions</a>
          <a className={`mx-2 font-semibold ${hovered ? 'text-gray-500' : 'text-black'} transition ease-in-out duration-300`} onMouseEnter={handleHover} onMouseLeave={handleHover} onClick={() => router.push('/food')}>Food</a>
          <a className={`px-4 py-2 rounded-full border-black ${hovered ? 'bg-gray-500 text-white' : 'text-black'} transition ease-in-out duration-300`} onMouseEnter={handleHover} onMouseLeave={handleHover} onClick={handleLogout}>Log Out</a>
        </div>
      </div>
    </nav>
  );
}
