import React, { useState, useEffect } from "react";

import axios from "axios";
import { reviewPage } from "../lib/auth";
import Rating from '@mui/material/Rating';
import Box from '@mui/material/Box';
import StarIcon from '@mui/icons-material/Star';
import { createClient } from '@supabase/supabase-js';

const supabase = createClient('https://xpqjwcqzcnzxjmujjdmc.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InhwcWp3Y3F6Y256eGptdWpqZG1jIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODIzMTU4OTcsImV4cCI6MTk5Nzg5MTg5N30.08k0Tjxtig93CR20VHzUdZaysWzE7NXggLOvMm5fHaM');

export default function ReviewForm() {
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [selectedMovie, setSelectedMovie] = useState("");
  const [movieData, setMovieData] = useState([]);
  const [movieDescription, setMovieDescription] = useState("");
  const [imageSrc, setImageSrc] = useState("");

  useEffect(() => {
    async function fetchMovies() {
      let { data: movies, error } = await supabase
        .from("movies")
        .select("*");
      if (error) console.log("Error fetching movies:", error);
      setMovieData(movies);
    }
    fetchMovies();
  }, []);

  const handleRatingChange = (value) => {
    setRating(value);
  };

  const handleReviewChange = (event) => {
    setReview(event.target.value);
  };

  const handleSelectionChange = (event) => {
    setSelectedMovie(event.target.value);
    const selectedMovieData = movieData.find((movie) => movie.movie_name === event.target.value);
    setMovieDescription(selectedMovieData.movie_description);
    setImageSrc(selectedMovieData.movie_url);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    await supabase.from("user_rating_review").insert([
      {
        movie_name: selectedMovie,
        rating: rating,
        review: review,
      },
    ]);
    alert("Review submitted successfully!");
    setRating(0);
    setReview("");
    setSelectedMovie("");
    setImageSrc("");
    setMovieDescription("");
  };

  return (
    <div className="container">
      <div className="left">
        {imageSrc && <img src={imageSrc} alt="Your Image" />}
        <h2>{selectedMovie}</h2>
        {movieDescription && <p>{movieDescription}</p>}
      </div>
      <div className="right">
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="selection">Select a Movie:</label>
            <select id="selection" value={selectedMovie} onChange={handleSelectionChange}>
              <option value="">Movie you watched</option>
              {movieData.map((movie) => (
<option key={movie.id} value={movie.movie_name}>
{movie.movie_name}
</option>
))}
</select>
</div>
<div>
  <label htmlFor="rating">Rate the movie:</label>
  <HoverRating value={rating} onChange={handleRatingChange} />
</div>

<div>
  <label htmlFor="review">Review:</label>
  <br />
  <textarea
    id="review"
    value={review}
    onChange={handleReviewChange}
    rows="5"
    placeholder="Enter your review here..."
  />
</div>

<button type="submit">Submit</button>
</form>
</div>
</div>
);
function HoverRating(props) {
  const [hover, setHover] = useState(-1);
  const handleHover = (event, newHover) => {
  setHover(newHover);
  };
  return (
  <Box sx={{  display: 'flex', alignItems: 'center' }}>
  <Rating
  name="hover-feedback"
  value={props.value}
  precision={0.5}
  onChange={(event, newValue) => {
  props.onChange(newValue);
  }}
  onChangeActive={handleHover}
  emptyIcon={<StarIcon style={{ opacity: 0.50 }} fontSize="inherit"  />}
  />
  {props.value !== null && (
  <Box sx={{ ml: 2 }}>{hover !== -1 ? hover : props.value}</Box>
  )}
  </Box>
  );
  }
}
