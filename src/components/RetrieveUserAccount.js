import { useState, useEffect } from 'react'
import { createClient } from '@supabase/supabase-js'
import Select from 'react-select'

const supabase = createClient(
  "https://xpqjwcqzcnzxjmujjdmc.supabase.co",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InhwcWp3Y3F6Y256eGptdWpqZG1jIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODIzMTU4OTcsImV4cCI6MTk5Nzg5MTg5N30.08k0Tjxtig93CR20VHzUdZaysWzE7NXggLOvMm5fHaM"
);

export default function UserList() {
  const [selectedOption, setSelectedOption] = useState(null)
  const [users, setUsers] = useState([])

  const options = [
    { value: 'cinema_manager', label: 'Cinema Manager' },
    { value: 'cinema_owner', label: 'Cinema Owner' },
    { value: 'customer_account', label: 'Customer' },
    { value: 'user_admin', label: 'Admin' },
  ]

  useEffect(() => {
    async function fetchUsers() {
      if (selectedOption) {
        let table = selectedOption.value
        if (selectedOption.value === 'customer_account') {
          
        }
        const { data, error } = await supabase
          .from(table)
          .select('*')
        if (error) console.log('Error fetching users:', error)
        else setUsers(data)
      } else {
        setUsers([])
      }
    }
    fetchUsers()
  }, [selectedOption])

  return (
    <div className="container2">
    <div>
      <h1>User Account</h1>
      <Select
        options={options}
        value={selectedOption}
        onChange={setSelectedOption}
      />
      {users.length > 0 ? (
        <table style={{ border: '1px solid black', borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            <th style={{ border: '1px solid black', padding: '5px' }}>Role of the user account</th>
            {selectedOption.value === 'customer_account' && (
              <>
                <th style={{ border: '1px solid black', padding: '5px' }}>Email</th>
                <th style={{ border: '1px solid black', padding: '5px' }}>Password</th>
                <th style={{ border: '1px solid black', padding: '5px' }}>First Name</th>
                <th style={{ border: '1px solid black', padding: '5px' }}>Last Name</th>
                <th style={{ border: '1px solid black', padding: '5px' }}>Date of Birth</th>
                <th style={{ border: '1px solid black', padding: '5px' }}>Telephone</th>
              </>
            )}
            {selectedOption.value !== 'customer_account' && (
              <>
                <th style={{ border: '1px solid black', padding: '5px' }}>Email</th>
                <th style={{ border: '1px solid black', padding: '5px' }}>Password</th>
              </>
            )}
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.email}>
              <td style={{ border: '1px solid black', padding: '5px' }}>{selectedOption.label}</td>
              {selectedOption.value === 'customer_account' && (
                <>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.email_username}</td>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.password}</td>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.first_name}</td>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.last_name}</td>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.dob}</td>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.telephone}</td>
                </>
              )}
              {selectedOption.value !== 'customer_account' && (
                <>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.email_username}</td>
                  <td style={{ border: '1px solid black', padding: '5px' }}>{user.password}</td>
                </>
              )}
            </tr>
          ))}
        </tbody>
      </table>
      
  ) : (
    <p>No users found</p>
  )}
</div>
</div>
)
}