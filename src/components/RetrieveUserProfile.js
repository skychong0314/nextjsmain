import { useState, useEffect } from 'react'
import { createClient } from '@supabase/supabase-js'
import Select from 'react-select'

const supabase = createClient(
  "https://xpqjwcqzcnzxjmujjdmc.supabase.co",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InhwcWp3Y3F6Y256eGptdWpqZG1jIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODIzMTU4OTcsImV4cCI6MTk5Nzg5MTg5N30.08k0Tjxtig93CR20VHzUdZaysWzE7NXggLOvMm5fHaM"
);

export default function UserList() {
  const [selectedOption, setSelectedOption] = useState(null)
  const [users, setUsers] = useState([])

  const options = [
    { value: 'cinema_manager', label: 'Cinema Manager' },
    { value: 'cinema_owner', label: 'Cinema Owner' },
    { value: 'customer_account', label: 'Customer' },
    { value: 'user_admin', label: 'Admin' },
  ]

  useEffect(() => {
    async function fetchUsers() {
      if (selectedOption) {
        const { data, error } = await supabase
          .from(selectedOption.value)
          .select('email_username')
        if (error) console.log('Error fetching users:', error)
        else setUsers(data)
      } else {
        setUsers([])
      }
    }
    fetchUsers()
  }, [selectedOption])

  return (
    <div className="container2">
    <div>
      <h1>User Profile</h1>
      <Select
        options={options}
        value={selectedOption}
        onChange={setSelectedOption}
      />
      {users.length > 0 ? (
        <ul>
          {users.map(user => (
            <li key={user.email_username}>
              <div className="container3">
              Name: {user.email_username} - Role: {selectedOption.label} 
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <p>No users to show</p>
      )}
    </div>
    </div>
  )
}
