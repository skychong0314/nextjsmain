import { useState } from 'react';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';

const MAX_RATING = 5;

export default function HoverRating(props) {
  const { onChange } = props;
  const [hover, setHover] = useState(null);
  const [rating, setRating] = useState(0);

  const handleHover = (value) => {
    setHover(value);
  };

  const handleClick = (value) => {
    setRating(value);
    if (onChange) {
      onChange(value);
    }
  };

  return (
    <div>
      {[...Array(MAX_RATING)].map((_, index) => {
        const value = index + 1;
        return (
          <span
            key={value}
            onMouseEnter={() => handleHover(value)}
            onMouseLeave={() => handleHover(null)}
            onClick={() => handleClick(value)}
          >
            {value <= (hover || rating) ? (
              <StarIcon color="primary" />
            ) : (
              <StarBorderIcon color="primary" />
            )}
          </span>
        );
      })}
    </div>
  );
}
